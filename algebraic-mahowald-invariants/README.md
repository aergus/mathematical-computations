# Algebraic Mahowald invariants

This directory contains some code for computing algebraic Mahowald invariants (also called
algebraic _root_ invariants).
The subdirectories correspond to different software that can be used to compute Ext over
the mod 2 Steenrod algebra.

* `ami-ext` uses [`ext`][bruner-papers].
It computes all the maps required to figure out the Mahowald filtrations of Ext classes,
but currently doesn't do the linear algebra to extract the Mahowald invariants from that
information.
* `ami-sseq` uses the [`sseq`][sseq] library.
It computes Mahowald invariants including the indeterminacy. 

## What are algebraic Mahowald invariants?

Here is a brief overview of what algebraic Mahowald invariants are, for details and background see
for instance the paper [The root invariant in homotopy theory][mahowald--ravenel].
In the following, we abbreviate the Ext groups `Ext^{s,t}_A(-, F_2)` over the mod 2 Steenrod
algebra as `Ext^{s,t}(-)`.

Let `M_k` be the cohomology of the stunted projective spectrum `RP_-k_inf`.
There is an isomorphism  `Ext^{s, t}(F_2) ~ lim_k Ext^{s, t-1}(M_k)` induced by the (-1)-cell
`S^{-1} -> RP_-k_inf` at each level.
Let `x` be a class in `Ext^{s, t}(F_2)`.
Then there is a minimal `k` such that its image in `Ext^{s, t-1}(M_k)` is non-trivial.
Using the long exact sequence induced by the (co)fiber sequence
`S^{-k} -> RP_-k_inf -> RP_{-k+1}_inf` on the level of `Ext`, that image can be lifted to a
class `M(x)` in `Ext^{s, t + k - 1}`, which is (a representative for) the *(algebraic) Mahowald
invariant of `x`*.


[sseq]: https://github.com/SpectralSequences/sseq
[bruner-papers]: http://www.rrb.wayne.edu/papers/
[mahowald--ravenel]: https://doi.org/10.1016/0040-9383(93)90055-Z
