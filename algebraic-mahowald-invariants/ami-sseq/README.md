# `ami-sseq`

A program that uses the [`sseq`][sseq] library to compute algebraic Mahowald invariants.

## Installation

This program uses a build process that is fairly standard for Rust projects.
Once you have a Rust toolchain [installed][install-rust], you should be able to simply run
`cargo build --release` to build it.

## Example usage

The following will compute the algebraic Mahowald invariants of classes in positive stems that are
detected in `RP_-25_inf` or before.
It will use a resolution for `S_2` located in the directory `S_2` and resolutions for
`RP_-k_inf`s located in `RP_-k_inf/RP_-[given k]_inf`, or compute and save those resolutions if
necessary.

```
cargo run --release -- -k 25 --s-2-path S_2 --p-k-prefix RP_-k_inf
```

The output (`stdout`) will look as follows.

```
class_s, class_t, class_i, k, mahowald_invariant, indeterminacy
1, 2, 0, 3, "[(1, 4, 0)]", "[]"
1, 4, 0, 5, "[(1, 8, 0)]", "[]"
2, 4, 0, 5, "[(2, 8, 0)]", "[]"
2, 5, 0, 6, "[(2, 10, 0)]", "[]"
-- omitting some lines --
5, 23, 0, 24, "[(5, 46, 0)]", "[]"
6, 23, 0, 24, "[(6, 46, 0), (6, 46, 1)]", "[]"
7, 23, 0, 24, "[(7, 46, 0)]", "[]"
3, 24, 0, 25, "[(3, 48, 0)]", "[]"
4, 24, 0, 25, "[(4, 48, 0)]", "[]"
6, 20, 0, 25, "[(6, 44, 0)]", "[[(6, 44, 1)]]"
```

It uses `sseq`'s naming conventions:
`(s, t, i)` stands for the `i`th generator in bidegree `(s, t)`.
Sums are represented as lists of basis elements and bases of indeterminacies as lists of such
lists.

## Options

Here is the help output providing an overview of the available options.

```
Compute 2-primary algebraic Mahowald invariants of Ext classes in positive stems

Usage: ami-sseq [OPTIONS] <--k-max <K_MAX>|--n-max <N_MAX>>

Options:
  -k, --k-max <K_MAX>              Try to detect classes in RP_-k_inf's up to k=K_MAX [positive, default: 2*N_MAX+1]
  -n, --n-max <N_MAX>              Limit the stem of the classes whose Mahowald invariants are computed [positive, default: K_MAX-1]
  -s, --s-max <S_MAX>              Limit the filtration of the classes whose Mahowald invariants are computed [positive, default: (N_MAX+1)/2+1]
  -t, --t-extension <T_EXTENSION>  Go T_EXTENSION steps further in the t direction beyond the n=N_MAX line [default: 0]
      --s-2-path <S_2_PATH>        Save directory for S_2
  -l, --lazy-quasi-inverses        Load the quasi-inverses of the differentials in the resolution for S_2 lazily (requires S_2_PATH)
      --p-k-prefix <P_K_PREFIX>    Directory containing save directories for RP_-k_inf's
  -p, --parallel-ks <N>            Process N RP_-k_inf's in parallel [default: 1]
  -h, --help                       Print help information
  -V, --version                    Print version information
```

[install-rust]: https://www.rust-lang.org/tools/install
[sseq]: https://github.com/SpectralSequences/sseq/
