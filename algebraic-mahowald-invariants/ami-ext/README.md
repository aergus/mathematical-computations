# `ami-ext`

A script that uses [`ext`][bruner-papers] to compute algebraic Mahowald filtrations.

## Requirements

The script requires the Python 3 standard library.
Moreover, it uses the programs in the `A` directory of a standard `ext` installation.

## Usage

The script should be run in the `A` directory of the `ext` distribution.
It assumes that an appropriate resolution of `F2` is present.
(The one that is provided with `ext` 1.9.5 should be fine for computing Mahowald filtrations of
classes through stem ~33.)
It generates module directories for `P_-k_[some large number]`s, and computes the required
resolutions and maps.

The output is written to `ami-ext.csv` and looks as follows.

```
class_s, class_t, class_g, k, mahowald_invariant_g
1, 2, 1, 3, 2
1, 4, 2, 5, 3
2, 4, 1, 5, 3
2, 5, 2, 6, 5
3, 6, 1, 7, 4
1, 8, 3, 9, 4
2, 8, 3, 9, 6
2, 9, 4, 10, 8
2, 10, 5, 11, 9
3, 10, 2, 11
3, 11, 3, 12, 9
4, 11, 1, 12, 7
3, 12, 4, 13, 10
-- more lines --
```

The first and the third column determine the `ext` name `s_g` of a basis element of `Ext_A(F2, F2)`.
The fourth column is the smallest `k` for which the elements maps non-trivially to the Ext for
`P_-k_inf`.
The maps are sometimes simple enough to see that `M(s_g)` contains a basis element `s_{g'}`
without doing significant linear algebra, which is indicated in the last column.

The script does not accept any command line arguments, but its behavior, in particular the stems to
consider, can be changed by adjusting the constants defined in its beginning.


[bruner-papers]: http://www.rrb.wayne.edu/papers/
